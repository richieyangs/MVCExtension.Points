using System.Web.Routing;

namespace MVCExtension.Points.Extensions.Helpers
{
    public struct MvcRequestModel
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public RouteValueDictionary RouteValuesDictionary { get; set; }
    }
}