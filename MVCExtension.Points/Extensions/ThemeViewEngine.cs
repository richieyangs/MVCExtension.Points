﻿using System.Web.Mvc;

namespace MVCExtension.Points.Extensions
{
    public class ThemeViewEngine: RazorViewEngine
    {
        public ThemeViewEngine(string theme)
        {

            ViewLocationFormats = new[]
            {
                "~/Views/Themes/" + theme + "/{1}/{0}.cshtml",
                "~/Views/Themes/" + theme + "/Shared/{0}.cshtml"
            };

            PartialViewLocationFormats = new[]
            {
                "~/Views/Themes/" + theme + "/{1}/{0}.cshtml",
                "~/Views/Themes/" + theme + "/Shared/{0}.cshtml"
            };

            AreaViewLocationFormats = new[]
            {
                "~Areas/{2}/Views/Themes/" + theme + "/{1}/{0}.cshtml",
                "~Areas/{2}/Views/Themes/" + theme + "/Shared/{0}.cshtml"
            };

            AreaPartialViewLocationFormats = new[]
            {
                "~Areas/{2}/Views/Themes/" + theme + "/{1}/{0}.cshtml",
                "~Areas/{2}/Views/Themes/" + theme + "/Shared/{0}.cshtml"
            };
        }
    }
}