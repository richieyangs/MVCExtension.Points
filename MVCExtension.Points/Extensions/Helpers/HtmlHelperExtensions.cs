﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace MVCExtension.Points.Extensions.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString ActionLink<TModel, TController>(this HtmlHelper<TModel> html, string linkText, Expression<Func<TController, ActionResult>> actionSelector,
          IDictionary<string, object> additionalHtmlAttributesDictionary = null)
        {
            var routeValues = actionSelector.GetRouteValues();
            return html.ActionLink(linkText, routeValues.Action, routeValues.Controller, routeValues.RouteValuesDictionary, additionalHtmlAttributesDictionary);
        }

        public static void RenderAction<TController>(this HtmlHelper html, Expression<Func<TController, ActionResult>> actionSelector)
        {
            var routeValues = actionSelector.GetRouteValues();
            html.RenderAction(routeValues.Action, routeValues.Controller, routeValues.RouteValuesDictionary);
        }
        
    }
}