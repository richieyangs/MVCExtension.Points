﻿using System.Web;
using System.Web.Mvc;
using MVCExtension.Points.Extensions;

namespace MVCExtension.Points
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new StopwatchAttribute());
            filters.Add(new Log4NetExceptionFilter());
            filters.Add(new TimeOutAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
