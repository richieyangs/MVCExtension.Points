﻿using System.Web.Mvc;
using log4net;

namespace MVCExtension.Points.Extensions
{
    public class Log4NetExceptionFilter : IExceptionFilter
    {
        private readonly ILog _logger;

        public Log4NetExceptionFilter()
        {
            _logger = LogManager.GetLogger(GetType());
        }
        public void OnException(ExceptionContext context)
        {
            _logger.Error("Unhandled exception", context.Exception);
        }
    }
}