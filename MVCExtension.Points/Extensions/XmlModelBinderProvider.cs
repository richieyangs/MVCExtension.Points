﻿using System;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Points.Extensions
{
    public class XmlModelBinderProvider: IModelBinderProvider
    {
        public IModelBinder GetBinder(Type modelType)
        {
            var contentType = HttpContext.Current.Request.ContentType.ToLower();
            if (contentType != "text/xml")
            {
                return null;
            }

            return new XmlModelBinder();
        }
    }
}