﻿using System.ComponentModel.DataAnnotations;

namespace MVCExtension.Points.Extensions
{
    public class AgeValidator: ValidationAttribute
    {
        public AgeValidator()
        {
            ErrorMessage = "Please enter the age>18";
        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return false;

            int age;
            if (int.TryParse(value.ToString(), out age))
            {
                if (age > 18)
                    return true;

                return false;
            }

            return false;
        }
    }
}