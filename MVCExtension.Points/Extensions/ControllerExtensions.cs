﻿using System.Web.Mvc;

namespace MVCExtension.Points.Extensions
{
    public static class ControllerExtensions
    {
        public static XmlResult XmlResult(this IController @this, object model)
        {
            return new XmlResult(model);
        }
    }
}