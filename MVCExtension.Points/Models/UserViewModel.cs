﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MVCExtension.Points.Extensions;

namespace MVCExtension.Points.Models
{
    public class UserViewModel:IValidatableObject
    {
        public string Name { get; set; }
        public string Email { get; set; }

        [Required]
        [AgeValidator]
        public int? Age { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Host { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(string.IsNullOrEmpty(Name))
                yield return new ValidationResult("the name can not be empty");

            if (Name.Equals("lucy"))
            {
                if(Age.Value<25)
                    yield return new ValidationResult("lucy's age must greater than 25");
            }
        }
    }
}