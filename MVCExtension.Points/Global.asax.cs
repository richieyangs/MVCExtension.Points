﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Windsor;
using MVCExtension.Points.Extensions;
using MVCExtension.Points.Plumbing;

namespace MVCExtension.Points
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["Theme"]))
            {
                var activeTheme = ConfigurationManager.AppSettings["Theme"];
                ViewEngines.Engines.Insert(0, new ThemeViewEngine(activeTheme));
            };
            ModelBinderProviders.BinderProviders.Insert(0, new XmlModelBinderProvider());
            ValueProviderFactories.Factories.Insert(0, new HttpValueProviderFactory());


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            FilterProviders.Providers.Add(new StopwatchFilterProvider());

        }
    }
}
