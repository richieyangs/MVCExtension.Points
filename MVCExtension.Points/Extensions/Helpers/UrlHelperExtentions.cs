﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace MVCExtension.Points.Extensions.Helpers
{
    public static class UrlHelperExtentions
    {
        public static string Action<TController>(
        this UrlHelper url,
        Expression<Func<TController, ActionResult>> actionSelector,
        string protocol = null,
        string hostname = null)
        {
            var routeValues = actionSelector.GetRouteValues();
            return url.Action(routeValues.Action, routeValues.Controller, routeValues.RouteValuesDictionary, protocol, hostname);
        } 
    }
}