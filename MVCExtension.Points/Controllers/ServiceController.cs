﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web.Mvc;
using MVCExtension.Points.Extensions;
using MVCExtension.Points.Models;
using MVCExtension.Points.Providers;

namespace MVCExtension.Points.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IUserProvider _userProvider;

        public ServiceController(IUserProvider userProvider)
        {
            _userProvider = userProvider;
        }

        
        //public void GetUser()
        //{
        //    var user = new UserViewModel()
        //    {
        //        Name = "richie",
        //        Age = 20,
        //        Email = "abc@126.com",
        //        Phone = "139********",
        //        Address = "my address"
        //    };
        //    XmlSerializer serializer = new XmlSerializer(typeof(UserViewModel));
        //    Response.ContentType = "text/xml";
        //    serializer.Serialize(Response.Output, user);
        //}

        //1.example of ActionResult extension
        public XmlResult GetUser()
        {
            var user = new UserViewModel()
            {
                Name = "richie",
                Age = 20,
                Email = "abc@126.com",
                Phone = "139********",
                Address = "my address"
            };

            return new XmlResult(user);
        }

        //example of ActionResult extension
        public CsvResult DownloadUser()
        {
            var user = new UserViewModel()
            {
                Name = "richie",
                Age = 20,
                Email = "abc@126.com",
                Phone = "139********",
                Address = "my address"
            };

            return new CsvResult(new List<UserViewModel>() { user }, "user");
        }

        //2.example of IExceptionFilter extension
        public ActionResult ThrowException()
        {
            throw new InvalidDataException("throw some exceptions...");
        }

        //3.example of htmlHelper extension
        public ActionResult GetProducts()
        {
            var products = new ProductListViewModel()
            {
                FoodProducts = new List<ProductListViewModel.Product>()
                {
                    new ProductListViewModel.Product("Popcorn",""),
                    new ProductListViewModel.Product("Coke",""),
                    new ProductListViewModel.Product("Egg",""),
                    new ProductListViewModel.Product("Meet","")
                },
                BookProducts = new List<ProductListViewModel.Product>()
                {
                    new ProductListViewModel.Product("c#",""),
                    new ProductListViewModel.Product("java",""),
                    new ProductListViewModel.Product("swift",""),
                    new ProductListViewModel.Product("c++","")
                },
                SportProducts = new List<ProductListViewModel.Product>()
                {
                      new ProductListViewModel.Product("Basketbal",""),
                    new ProductListViewModel.Product("Football",""),
                    new ProductListViewModel.Product("Table tennis ball",""),
                    new ProductListViewModel.Product("Badminton","")
                }
            };
            return View(products);
        }

        //4.example of ViewEngine extension
        public ActionResult SomeView()
        {
            return View();
        }

        //5.example of Model validator extension
        public ActionResult InputAge()
        {
            return View();
        }
            
        [HttpPost]
        public ActionResult InputAge(UserViewModel user)
        {
            if (ModelState.IsValid)
            {
                return View("SaveAge",user);
            }

            return View();
        }

        //6.example of xml model binder extension
        //public ActionResult PostXmlContent([ModelBinder(typeof(XmlModelBinder))]UserViewModel user)
        //{
        //    return new XmlResult(user);
        //}

        public ActionResult PostXmlContent(UserViewModel user)
        {
            return new XmlResult(user);
        }

        //7.example of Controller Factory extension
        public ActionResult GetUserByIoc()
        {
            var user = _userProvider.GetUser();
            return new XmlResult(user);
        }

        //8.example of Lambda Tree helper
        public ActionResult Orders()
        {
            return View();
        }

        public ActionResult OrderLineItem(int id)
        {
            return View(id);
        }

        //example of value provider extension
        public ActionResult SaveUser(UserViewModel user)
        {
            //user.Host = Request.Host;
            return Content(user.Host);
        }

       
    }
}