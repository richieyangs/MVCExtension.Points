﻿using System.Diagnostics;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace MVCExtension.Points.Extensions
{
    public class MyAuthenticationAttribute:FilterAttribute,IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            Debug.Write("call OnAuthentication");

            if (!filterContext.Principal.Identity.IsAuthenticated)
                filterContext.Result = new HttpUnauthorizedResult();
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            Debug.Write("call OnAuthenticationChallenge");
        }
    }

    public class MyAuthorizationAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            Debug.Write("call OnAuthorization");

            IPrincipal user = filterContext.HttpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }
    }
}