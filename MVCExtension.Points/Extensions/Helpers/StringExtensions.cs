﻿namespace MVCExtension.Points.Extensions.Helpers
{
    public static class StringExtensions
    {
        public static string FormatWith(this string @this, params object[] paraStrings)
        {
            return string.Format(@this, paraStrings);
        }
    }
}
