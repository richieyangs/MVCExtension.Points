﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MVCExtension.Points.Extensions
{
    public class StopwatchFilterProvider : IFilterProvider
    {
        public IEnumerable<Filter> GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            return new[] { new Filter(new StopwatchAttribute(), FilterScope.Global, 0) };
        }
    }
}