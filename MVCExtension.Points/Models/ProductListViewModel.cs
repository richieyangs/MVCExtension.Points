﻿using System.Collections.Generic;

namespace MVCExtension.Points.Models
{
    public class ProductListViewModel
    {
        public List<Product> FoodProducts { get; set; }
        public List<Product> BookProducts { get; set; }
        public List<Product> SportProducts { get; set; }

        public ProductListViewModel()
        {
            FoodProducts=new List<Product>();
            BookProducts = new List<Product>();
            SportProducts = new List<Product>();
        } 
        
        public class Product
        {
            public Product(string name, string href)
            {
                Name = name;
                Href = href;
            }
            public string Name { get; set; }
            public string Href { get; set; }

        }
    }
}