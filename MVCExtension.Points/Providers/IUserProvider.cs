﻿using MVCExtension.Points.Models;

namespace MVCExtension.Points.Providers
{
    public interface IUserProvider
    {
        UserViewModel GetUser();
    }

    public class UserProvider : IUserProvider
    {
        public UserViewModel GetUser()
        {
            var user = new UserViewModel()
            {
                Name = "richie",
                Age = 20,
                Email = "abc@126.com",
                Phone = "139********",
                Address = "my address"
            };

            return user;
        }
    }
}